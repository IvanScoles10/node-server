'use strict';

const bodyParser = require('body-parser');
const express = require('express');
const path = require('path');
const socketIO = require('socket.io');
const uuid = require('uuid');

const PORT = process.env.PORT || 3001;
const INDEX = path.join(__dirname, 'index.html');

// const analyticsService = require('./services/analytics-service');
// const cron = require('node-cron');
// var analytics = {};
// CRON TO HANDLE STATISTICS
// cron.schedule('0 23 * * *', function () {
//     messagesService.getCountsByStatus(function (err, response) {
//         if (!err) {
//             analytics = {};
//             analytics.open = _.get(_.find(response, { _id : 'open' }), 'count', 0);
//             analytics.inProgress = _.get(_.find(response, { _id : 'in-progress' }), 'count', 0);
//             analytics.closed =  _.get(_.find(response, { _id : 'closed' }), 'count', 0);

//             analyticsService.updateFeedback(analytics, function (err, response) {
//                 if (!err) {
//                     console.log('Cron running successfuly, messages statistics updated');
//                 }
//             });
//         }
//     })
// });

const app = express();
const server = app
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.json())
    .use(function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

        next();
    })
    .listen(PORT, () => console.log(`Listening on ${ PORT }`));

global.server = server;

// global[uuid].io = socketIO(server);
// global[uuid].io.on('connection', function (socket) {
//     console.log('Client connected', socket.id);
//     socket.on('disconnect', () => console.log('Client disconnected'));
// });

app.get('/', function (req, res) {
    res.sendFile(INDEX);
});

app.use('/api/v1/', require('./routes/webhook'));
app.use('/api/v1/', require('./routes/message'));
app.use('/api/v1/', require('./routes/category'));
app.use('/api/v1/', require('./routes/template'));
