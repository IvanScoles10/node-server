const uuid = require('uuid');
const groupsMongo = require('../mongo/groups-mongo');

const groupsService = {

    getAll: function (cb) {
        groupsMongo.getAll(function (err, groups) {
            if (!err) {
                return cb(null, groups);
            } else {
                return cb(err);
            }
        });
    },

    get: function (id, cb) {
        groupsMongo.get(id, function (err, groups) {
            if (!err) {
                return cb(null, groups);
            } else {
                return cb(err);
            }
        });
    },

    getByName: function (name, cb) {
        groupsMongo.getByName(name, function (err, groups) {
            if (!err) {
                return cb(null, groups);
            } else {
                return cb(err);
            }
        });
    },

    create: function (data, cb) {
        groupsMongo.getByName(data.name, function (err, group) {
            if (!err) {
                if (!group) {
                    groupsMongo.create({
                        name: data.name,
                        users: data.users
                    }, function (err, response) {
                        if (!err) {
                            global[uuid].io.sockets.emit('client:get_groups');

                            return cb(null, response);
                        } else {
                            return cb({ error: { code: 'create_groups_mongo' } });
                        }
                    });
                } else {
                    return cb({ error: { code: 'group_exists' } });
                }
            } else {
                return cb({ error: { code: 'get_groups_by_name_mongo' } });
            }
        });
    },

    update: function (data, cb) {
        groupsMongo.update(data, function (err, group) {
            if (!err) {
                return cb(null, group);
            } else {
                return cb(err);
            }
        });
    },

    delete: function (id, cb) {
        groupsMongo.delete(id, function (err, group) {
            if (!err) {
                global[uuid].io.sockets.emit('client:get_groups');

                return cb(null, { success: true });
            } else {
                return cb({ error: { code: 'delete_groups_mongo' } });
            }
        });
    },

    updateUsers: function (data, cb) {
        groupsMongo.updateUsers({
            id: data.id,
            user: data.user
        }, function (err, response) {
            if (!err) {
                global[uuid].io.sockets.emit('client:get_group_by_id', data.id);

                return cb(null, response);
            } else {
                return cb({ error: { code: 'update_users_groups_by_id' } });
            }
        });
    },

    deleteUsers: function (data, cb) {
        groupsMongo.deleteUsers({
            id: data.id,
            userId: data.userId
        }, function (err, response) {
            if (!err) {
                global[uuid].io.sockets.emit('client:get_group_by_id', data.id);

                return cb(null, response);
            } else {
                return cb({ error: { code: 'delete_users_groups_by_id' } });
            }
        });
    }
};

module.exports = groupsService;
