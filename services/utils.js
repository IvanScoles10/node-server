const utils = {
    retryOnce: function (func, recoverFunc) {
        return func()
            .catch(function(err) {
                return recoverFunc(err).then(() => func());
            });
    }
};

module.exports = utils;
