const moment = require('moment');

const templateMongo = require('../mongo/template-mongo');
const templateService = {

    getAll: function (company_id, cb) {
        templateMongo.getAll(company_id, function (err, templates) {
            if (!err) {
                return cb(null, templates);
            } else {
                return cb(err);
            }
        });
    },

    get: function (id, cb) {
        templateMongo.get(id, function (err, template) {
            if (!err) {
                return cb(null, template);
            } else {
                return cb(err);
            }
        });
    },

    getByName: function (name, cb) {
        templateMongo.getByName(name, function (err, template) {
            if (!err) {
                return cb(null, template);
            } else {
                return cb(err);
            }
        });
    },

    create: function (data, cb) {
        templateMongo.getByName(data.name, function (err, template) {
            if (!err) {
                if (!template) {
                    templateMongo.create({
                        company_id: data.company_id,
                        date_created: moment().unix(),
                        description: data.description,
                        name: data.name,
                        notes: (data.notes) ? data.notes : "",
                        text: data.text,
                    }, function (err, response) {
                        if (!err) {
                            return cb(null, response);
                        } else {
                            return cb({ error: { code: 'error:template-service:create' } });
                        }
                    });
                } else {
                    return cb({ error: { code: 'error:template-service:create:group_exist' } });
                }
            } else {
                return cb({ error: { code: 'error:template-service:create' } });
            }
        });
    },

    update: function (data, cb) {
        templateMongo.update(data, function (err) {
            if (!err) {
                return cb(null, data);
            } else {
                return cb({ error: { code: 'error:template-service:update' } });
            }
        });
    },

    delete: function (id, cb) {
        templateMongo.delete(id, function (err) {
            if (!err) {
                return cb(null, { success: true });
            } else {
                return cb({ error: { code: 'error:template-service:delete' } });
            }
        });
    },
};

module.exports = templateService;
