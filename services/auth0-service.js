const _ = require('lodash');
const env = require('../environment/env');
const uuid = require('uuid');
var authMongo = require('../mongo/auth-mongo');
var request = require('request-promise');
var options = {};
var self;
var refreshPromise;
var utils = require('./utils');
var error;

const auth0Service = {
    getAuth0AccessToken: function () {
        options = {
            url: env.auth0.API + env.auth0.GET_ACCESS_TOKEN,
            headers: {
                'Authorization': 'Basic Og==',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                audience: env.auth0.AUDIENCE,
                client_id: env.auth0.CLIENT_ID,
                client_secret: env.auth0.CLIENT_SECRET,
                grant_type: env.auth0.GRANT_TYPE
            })
        };

        return request.post(options);
    },

    getAccessToken: function () {
        self = this;

        if (refreshPromise) {
            return refreshPromise;
        } else {
            refreshPromise = new Promise(function (resolve, reject) {
                self.getAuth0AccessToken().then(function (data) {
                    data = JSON.parse(data);

                    authMongo.update(data, function (error, response) {
                        if (error) {
                            console.log('Error on update auth0 mongo');
                        }
                    });

                    resolve(data);
                }).catch(function (err) {
                    self.expireToken();
                    reject(err);
                });
            });

            return refreshPromise;
        }
    },

    expireToken: function () {
        refreshPromise = undefined;
    },

    refreshTokenOnUnauthorizedError: function (err) {
        if (JSON.stringify(err).includes('401')) {
            self.expireToken();
            return self.getAccessToken();
        } else {
            return Promise.reject(err);
        }
    },

    getUsers: function () {
        var self = this;

        return utils.retryOnce(function () {
            return self.getAccessToken()
                .then(function(response) {
                    options = {
                        url: env.auth0.API + env.auth0.GET_USERS,
                        headers: {
                            'Authorization': 'Bearer ' + response.access_token,
                            'Content-Type': 'application/json'
                        },
                    };

                    return request.get(options);
                })
                .catch(function (err) {
                    return Promise.reject(err);
                });
        }, self.refreshTokenOnUnauthorizedError);
    },

    createUser: function (data, cb) {
        var self = this;

        return utils.retryOnce(function () {
            return self.getAccessToken()
                .then(function(response) {
                    options = {
                        url: env.auth0.API + env.auth0.SIGN_UP,
                        headers: {
                            'Authorization': 'Bearer ' + response.access_token,
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            client_id: env.auth0.CLIENT_ID,
                            email: data.email,
                            password: data.password,
                            username: data.username,
                            user_metadata: _.get(data, 'user_metadata'),
                            connection: env.auth0.CONNECTION
                        })
                    };

                    request.post(options).then(function (response) {
                        cb(null, JSON.parse(response));
                    }).catch(function(err) {
                        error = JSON.parse(err.error);

                        if (error.statusCode !== '401') {
                            global[uuid].io.sockets.emit('client:get_users');

                            cb(error);
                        } else {
                            return Promise.reject(err);
                        }
                    });
                })
                .catch(function (err) {
                    return Promise.reject(err);
                });
        }, self.refreshTokenOnUnauthorizedError);
    },

    deleteUser: function (id, cb) {
        var self = this;

        return utils.retryOnce(function () {
            return self.getAccessToken()
                .then(function(response) {
                    options = {
                        url: env.auth0.API + env.auth0.DELETE + id,
                        headers: {
                            'Authorization': 'Bearer ' + response.access_token,
                            'Content-Type': 'application/json'
                        }
                    };

                    request.delete(options).then(function (response) {
                        cb(null, JSON.parse(response));
                    }).catch(function(err) {
                        error = JSON.parse(err.error);

                        if (error.statusCode !== '401') {
                            global[uuid].io.sockets.emit('client:get_users');

                            cb(error);
                        } else {
                            return Promise.reject(err);
                        }
                    });
                })
                .catch(function (err) {
                    return Promise.reject(err);
                });
        }, self.refreshTokenOnUnauthorizedError);
    }
};

module.exports = auth0Service;
