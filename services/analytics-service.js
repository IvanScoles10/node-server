const uuid = require('uuid');
const analyticsMongo = require('../mongo/analytics-mongo');

const analyticsService = {

    updateFeedback: function (data, cb) {
        analyticsMongo.updateFeedback(data, function (err, response) {
            if (!err) {
                return cb(null, response);
            } else {
                return cb(err);
            }
        });
    },
};

module.exports = analyticsService;
