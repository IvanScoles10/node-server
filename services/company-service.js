const moment = require('moment');

const companyMongo = require('../mongo/company-mongo');
const companyService = {

    getAll: function (cb) {
        companyMongo.getAll(function (err, companies) {
            if (!err) {
                return cb(null, companies);
            } else {
                return cb(err);
            }
        });
    },

    get: function (id, cb) {
        companyMongo.get(id, function (err, company) {
            if (!err) {
                return cb(null, company);
            } else {
                return cb(err);
            }
        });
    },

    getByName: function (name, cb) {
        companyMongo.getByName(name, function (err, company) {
            if (!err) {
                return cb(null, company);
            } else {
                return cb(err);
            }
        });
    },

    getByFacebookPageId: function (facebook_page_id, cb) {
        companyMongo.getByFacebookPageId(facebook_page_id, function (err, company) {
            if (!err) {
                return cb(null, company);
            } else {
                return cb(err);
            }
        });
    },

    create: function (data, cb) {
        companyMongo.getByName(data.name, function (err, company) {
            if (!err) {
                if (!company) {
                    companyMongo.create({
                        company_id: data.company_id,
                        date_created: moment().unix(),
                        facebook_page_id: data.facebook_page_id,
                        name: data.name,
                    }, function (err, response) {
                        if (!err) {
                            return cb(null, response.ops[0]);
                        } else {
                            return cb({ error: { code: 'error:company-service:create' } });
                        }
                    });
                } else {
                    return cb({ error: { code: 'error:company-service:create:group_exist' } });
                }
            } else {
                return cb({ error: { code: 'error:company-service:create' } });
            }
        });
    },

    update: function (data, cb) {
        companyMongo.update(data, function (err, response) {
            if (!err) {
                return cb(null, data);
            } else {
                return cb({ error: { code: 'error:company-service:update' } });
            }
        });
    },

    delete: function (id, cb) {
        companyMongo.delete(id, function (err) {
            if (!err) {
                return cb(null, { success: true });
            } else {
                return cb({ error: { code: 'error:company-service:delete' } });
            }
        });
    },

};

module.exports = companyService;
