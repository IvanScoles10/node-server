var _ = require('lodash');
var action = {};
var data = {};
var env = require('../environment/env');
var request = require('request-promise');
var body = {};
var button = {};
var buttons = [];
var options = {};

const facebookService = {
    getUserInfo: function (userId) {
        options = {
            url: env.facebook.GRAPH_API + '/' + userId + '?fields=first_name,last_name,profile_pic,locale,timezone,gender&access_token=' + env.facebook.ACCESS_TOKEN
        };

        return request.get(options);
    },

    setWelcomeMessage: function (data) {
        options = {
            url: env.facebook.GRAPH_API + '/me/thread_settings?access_token=' + env.facebook.ACCESS_TOKEN,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                setting_type: 'greeting',
                greeting: {
                    text: data.text
                }
            })
        };

        return request.post(options);
    },

    sendMessage: function (data) {
        options = {
            url: env.facebook.GRAPH_API + '/me/messages?access_token=' + env.facebook.ACCESS_TOKEN,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.transformMessageData(data))
        };

        return request.post(options);
    },

    transformMessageData: function (body) {
        data = {
            recipient: {
                id: body.sender_id
            },
            message: {
                text: body.message.text
            }
        };

        if (_.get(body.message, 'feedback')) {
            data.message = {
                attachment: {
                    type: 'template',
                    payload: {
                        template_type: 'button',
                        text: body.message.text,
                        buttons: this.getFeedbackButtons(body.message)
                    }
                }
            };
        }

        return data;
    },

    getFeedbackButtons: function (data) {
        action = {};
        buttons = [];

        _.map(data.feedback.actions, function (action, index) {
            action = data.feedback.actions[index];

            buttons.push({
                type: 'postback',
                title: action['action_message_' + index],
                payload: 'TYPE=FEEDBACK,ACTION=' + action['action_name_' + index]  +
                    ',FEEDBACK_ID=' + data.feedback._id
            });
        });

        return buttons;
    }
};

module.exports = facebookService;
