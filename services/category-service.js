const moment = require('moment');
const categoryMongo = require('../mongo/category-mongo');
const categoryService = {

    getAll: function (company_id, cb) {
        categoryMongo.getAll(company_id, function (err, categories) {
            if (!err) {
                return cb(null, categories);
            } else {
                return cb(err);
            }
        });
    },

    get: function (id, cb) {
        categoryMongo.get(id, function (err, category) {
            if (!err) {
                return cb(null, category);
            } else {
                return cb(err);
            }
        });
    },

    getByName: function (name, cb) {
        categoryMongo.getByName(name, function (err, category) {
            if (!err) {
                return cb(null, category);
            } else {
                return cb(err);
            }
        });
    },

    create: function (data, cb) {
        categoryMongo.getByName(data.name, function (err, category) {
            if (!err) {
                if (!category) {
                    categoryMongo.create({
                        company_id: data.company_id,
                        date_created: moment().unix(),
                        description: data.description,
                        name: data.name,
                        notes: (data.notes) ? data.notes : "",
                        users: (data.users) ? data.users : [],
                        keywords: (data.keywords) ? data.keywords : [],
                    }, function (err, response) {
                        if (!err) {
                            return cb(null, response);
                        } else {
                            return cb({ error: { code: 'error:category-service:create' } });
                        }
                    });
                } else {
                    return cb({ error: { code: 'error:category-service:create:group_exist' } });
                }
            } else {
                return cb({ error: { code: 'error:category-service:create' } });
            }
        });
    },

    update: function (data, cb) {
        categoryMongo.update(data, function (err, data) {
            if (!err) {
                return cb(null, data);
            } else {
                return cb({ error: { code: 'error:category-service:update' } });
            }
        });
    },

    delete: function (id, cb) {
        categoryMongo.delete(id, function (err) {
            if (!err) {
                return cb(null, { success: true });
            } else {
                return cb({ error: { code: 'error:category-service:delete' } });
            }
        });
    },

    updateUsers: function (data, cb) {
        categoryMongo.updateUsers({
            id: data.id,
            user: data.user
        }, function (err, response) {
            if (!err) {
                return cb(null, response);
            } else {
                return cb({ error: { code: 'error:category-service:updateUsers' } });
            }
        });
    },

    deleteUsers: function (data, cb) {
        categoryMongo.deleteUsers({
            id: data.id,
            user_id: data.user_id
        }, function (err, response) {
            if (!err) {
                return cb(null, response);
            } else {
                return cb({ error: { code: 'error:category-service:deleteUsers' } });
            }
        });
    }
};

module.exports = categoryService;
