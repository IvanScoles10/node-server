const _ = require('lodash');
const companyService = require('./company-service');
const facebookService = require('./facebook-service');
const messageMongo = require('../mongo/message-mongo');
const ObjectID = require('mongodb').ObjectID;

let feedback = null;
let payload = null;
let feedbackAction = null;
let feedbackId = null;

const messagesService = {

    getBySenderId: function (senderId, cb) {
        messageMongo.getBySenderId(senderId, function (err, message) {
            if (!err) {
                return cb(null, message);
            } else {
                return cb(err);
            }
        });
    },

    get: function (messageId, cb) {
        messageMongo.getAll(function (err, response) {
            if (!err) {
                response = _.find(response, { _id : new ObjectID(messageId) });
                return cb(null, response);
            } else {
                return cb(err);
            }
        });
    },

    getAll: function (filterBy, cb) {
        messageMongo.getAll(filterBy, function (err, data) {
            if (!err) {
                return cb({ messages: data });
            } else {
                return cb({ error: { code: 'error:messageService:getAll' } });
            }
        });
    },

    getById: function (id, cb) {
        messageMongo.get(id, function (err, data) {
            if (!err) {
                return cb({ message: data });
            } else {
                return cb({ error: { code: 'error:messageService:getById' } });
            }
        });
    },

    update: function (oldMessage, newMessage, messaging, userInfo) {
        if ((!this.isDuplicated(
            _.get(oldMessage, 'messages', []),
            _.get(newMessage, 'text', null)
        )) || _.get(newMessage, 'type') === 'sender') {
            if (oldMessage) {
                this.updateInteraction(oldMessage, newMessage);
            } else {
                this.createInteraction(messaging, newMessage, userInfo);
            }
        }
    },

    isDuplicated: function (messages, text) {
        return _.some(messages, function(message) {
            return (message.text === text);
        });
    },

    sendMessage: function (data, cb) {
        this.get(data.id, function (err, oldMessage) {
            if (oldMessage) {
                facebookService.sendMessage({
                    id: data.id,
                    sender_id: data.sender.id,
                    message: data.message
                }).then(function (response) {
                    if (!oldMessage.recipient) {
                        oldMessage = _.extend(oldMessage, {
                            recipient: data.recipient
                        });
                    }

                    oldMessage.messages.push(data.message);

                    messageMongo.update(oldMessage, function (err, data) {
                        if (!err) {
                            return cb(null, data);
                        } else {
                            return cb({
                                error: {
                                    code: 'error:messageService:update'
                                }
                            });
                        }
                    });
                }).catch(function (err) {
                    return cb({
                        error: {
                            code: 'error:messageService:sendMessage'
                        }
                    });
                });
            } else {
                return cb({
                    error: {
                        code: 'error:messageService:sendMessage'
                    }
                });
            }
        });
    },

    createInteraction: function (messaging, newMessage, userInfo) {
        // before create message we need to get here company id 
        // with facebook page id to have multiple messages by company
        companyService.getByFacebookPageId(newMessage.facebook_page_id, function (err, company) {
            if (!err) {
                sender = _.extend(messaging.sender, userInfo);
                data = {
                    company_id: company.company_id,
                    messages: [ newMessage ],
                    sender: sender,
                    status: 'OPEN',
                    read_by_recipient: false,
                };

                messageMongo.create(data, function (err, data) {
                    if (!err) {

                    } else {
                        console.log('error:messageService:createInteraction');
                    }
                });
            } else {
                console.log('error:messageService:getByFacebookPageId');
            }
        });
    },

    updateInteraction: function (oldMessage, newMessage) {
        oldMessage.messages.push(newMessage);

        messageMongo.update(oldMessage, function (err, data) {
            if (!err) {

            } else {
                console.log('error:messageService:updateInteraction');
            }
        });
    },

    updateStatusById: function (data, cb) {
        messageMongo.updateStatusById({
            id: data.id,
            status: data.status
        }, function (err, data) {
            if (!err) {
                return cb(null, data);
            } else {
                return cb({ error: { code: 'error:messageService:updateStatusById' } });
            }
        });
    },

    updateTagById: function (data, cb) {
        messageMongo.updateTagById({
            id: data.id,
            tag: data.tag
        }, function (err, data) {
            if (!err) {
                return cb(null, data);
            } else {
                return cb({ error: { code: 'error:messageService:updateTagById' } });
            }
        });
    },

    updateGroupById: function (data, cb) {
        messageMongo.updateGroupById({
            id: data.id,
            group: data.group
        }, function (err, data) {
            if (!err) {
                return cb(null, data);
            } else {
                return cb({ error: { code: 'error:messageService:updateGroupById' } });
            }
        });
    },

    updateOwnerById: function (data, cb) {
        messageMongo.updateOwnerById({
            id: data.id,
            owner: data.owner
        }, function (err, data) {
            if (!err) {
                return cb(null, data);
            } else {
                return cb({ error: { code: 'error:messageService:updateOwnerById' } });
            }
        });
    },

    getCountsByStatus: function (cb) {
        messageMongo.getCountsByStatus(function (err, data) {
            if (!err) {
                return cb(null, data);
            } else {
                return cb({ error: { code: 'error:messageService:getCountsByStatus:' } });
            }
        });
    },

    transformFeedback: function (postback) {
        payload = postback.payload;
        payload = payload.split(',');
        feedbackAction = payload[1];
        feedbackAction = feedbackAction.split('=');
        feedbackId =  payload[2];
        feedbackId = feedbackId.split('=');

        feedback = {
            _id: feedbackId[1],
            action: feedbackAction[1]
        };

        return feedback;
    },

    isFeedback: function (postback) {
        return _.includes(postback.payload, 'FEEDBACK');
    }
};

module.exports = messagesService;
