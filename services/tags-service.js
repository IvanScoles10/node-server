const uuid = require('uuid');
const TagsMongo = require('../mongo/tags-mongo');

const TagsService = {

    getAll: function (cb) {
        TagsMongo.getAll(function (err, tags) {
            if (!err) {
                return cb(null, tags);
            } else {
                return cb(err);
            }
        });
    },

    get: function (id, cb) {
        TagsMongo.get(id, function (err, tag) {
            if (!err) {
                return cb(null, tag);
            } else {
                return cb(err);
            }
        });
    },

    getByName: function (name, cb) {
        TagsMongo.getByName(name, function (err, tag) {
            if (!err) {
                return cb(null, tag);
            } else {
                return cb(err);
            }
        });
    },

    create: function (data, cb) {
        TagsMongo.getByName(data.name, function (err, tag) {
            if (!err) {
                if (!tag) {
                    TagsMongo.create({
                        name: data.name,
                        description: data.description
                    }, function (err, response) {
                        if (!err) {
                            global[uuid].io.sockets.emit('client:get_tags');

                            return cb(null, response);
                        } else {
                            return cb({ error: { code: 'create_tag_mongo' } });
                        }
                    });
                } else {
                    return cb({ error: { code: 'tag_exists' } });
                }
            } else {
                return cb({ error: { code: 'get_tag_by_name_mongo' } });
            }
        });
    },

    update: function (data, cb) {
        TagsMongo.update(data, function (err, tag) {
            if (!err) {
                return cb(null, tag);
            } else {
                return cb(err);
            }
        });
    },

    delete: function (id, cb) {
        TagsMongo.delete(id, function (err, tag) {
            if (!err) {
                global[uuid].io.sockets.emit('client:get_tags');

                return cb(null, { success: true });
            } else {
                return cb({ error: { code: 'delete_tag_mongo' } });
            }
        });
    }
};

module.exports = TagsService;
