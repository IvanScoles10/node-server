const uuid = require('uuid');
const feedbackMongo = require('../mongo/feedback-mongo');

const feedbackService = {

    getAll: function (cb) {
        feedbackMongo.getAll(function (err, groups) {
            if (!err) {
                return cb(null, groups);
            } else {
                return cb({ error: { code: 'get_feedback_mongo' } });
            }
        });
    },

    get: function (id, cb) {
        feedbackMongo.get(id, function (err, feedback) {
            if (!err) {
                return cb(null, feedback);
            } else {
                return cb({ error: { code: 'get_feedback_by_id_mongo' } });
            }
        });
    },

    getByName: function (name, cb) {
        feedbackMongo.getByName(name, function (err, feedback) {
            if (!err) {
                return cb(null, feedback);
            } else {
                return cb(err);
            }
        });
    },

    create: function (data, cb) {
        feedbackMongo.getByName(data.name, function (err, feedback) {
            if (!err) {
                if (!feedback) {
                    feedbackMongo.create(data, function (err, response) {
                        if (!err) {
                            global[uuid].io.sockets.emit('client:get_feedback');

                            return cb(null, response);
                        } else {
                            return cb({ error: { code: 'create_feedback_mongo' } });
                        }
                    });
                } else {
                    return cb({ error: { code: 'feedback_exists' } });
                }
            } else {
                return cb({ error: { code: 'get_feedback_by_name_mongo' } });
            }
        });
    },

    delete: function (id, cb) {
        feedbackMongo.delete(id, function (err, group) {
            if (!err) {
                global[uuid].io.sockets.emit('client:get_feedback');

                return cb(null, { success: true });
            } else {
                return cb({ error: { code: 'delete_feedback_mongo' } });
            }
        });
    }
};

module.exports = feedbackService;
