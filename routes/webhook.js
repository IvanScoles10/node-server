const _ = require('lodash');
const express = require('express');
const router = express.Router();

const facebookService = require('../services/facebook-service');
const messageService = require('../services/message-service');

let messaging = null;
let oldMessage = null;
let userInfo = null;
let newMessage = null;
let lastEntry = null;

router.get('/webhook', function (req, res) {
    if (req.query['hub.verify_token'] === 'chat_store') {
        res.status(200).send(req.query['hub.challenge']);
    } else {
        res.status(200).send({ error: 'Error, wrong validation token' });
    }
});

router.post('/webhook', function (req, res) {
    if (req.body.object === 'page') {        
        lastEntry = _.last(req.body.entry);
        messaging = lastEntry.messaging[0];

        if (_.get(messaging.message, 'text') || _.get(messaging.postback, 'title')) {
            newMessage = {
                mid: _.get(messaging.message, 'mid'),
                facebook_page_id: lastEntry.id,
                seq: _.get(messaging.message, 'seq'),
                text: _.get(messaging.message, 'text') || _.get(messaging.postback, 'title'),
                timestamp: messaging.timestamp,
            };

            if (_.get(messaging.postback, 'title')) {
                if (messageService.isFeedback(messaging.postback)) {
                    newMessage = _.extend(newMessage, {
                        feedback: messageService.transformFeedback(messaging.postback)
                    });
                }
            }

            facebookService.getUserInfo(messaging.sender.id).then(function (response) {
                userInfo = JSON.parse(response);
                newMessage = _.extend(newMessage, {
                    type: 'sender'
                });

                messageService.getBySenderId(
                    messaging.sender.id,
                    function (err, message) {
                        if (!err) {
                            if (message) {
                                oldMessage = message;

                                messageService.update(oldMessage, newMessage);
                            } else {
                                messageService.update(oldMessage, newMessage, messaging, userInfo);
                            }
                        } else {
                            console.log("error:webook:getBySenderId");
                        }
                    }
                );
            }).catch(function (err) {
                newMessage = _.extend(newMessage, {
                    type: 'recipient'
                });

                messageService.getBySenderId(
                    messaging.recipient.id,
                    function (err, message) {
                        if (err) {
                            if (message) {
                                oldMessage = message;

                                messageService.update(oldMessage, newMessage);
                            }
                        } else {
                            console.log("error:webhook:messageService");
                        }
                    }
                );
            });
        }
    }
    
    res.sendStatus(200);
});

module.exports = router;
