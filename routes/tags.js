const express = require('express');
const router = express.Router();

const TagsService = require('../services/tags-service');

router.get('/tags', function (req, res) {
    TagsService.getAll(function (err, data) {
        if (!err) {
            res.send({ tags: data });
        } else {
            res.send(err);
        }
    });
});

router.get('/tags/:id', function (req, res) {
    if (req.params.id) {
        TagsService.get(req.params.id, function (err, data) {
            if (!err) {
                res.send({ tag: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.post('/tags', function (req, res) {
    if (req.body.name && req.body.description) {
        TagsService.create({
            name: req.body.name,
            description: req.body.description
        }, function (err, data) {
            if (!err) {
                res.send({ tag: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.delete('/tags/:id', function (req, res) {
    if (req.params.id) {
        TagsService.delete(req.params.id, function (err, data) {
            if (!err) {
                res.send(data);
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.put('/tags/:id', function (req, res) {
    if (req.params.id && req.body.name && req.body.description) {
        TagsService.updateUsers({
            id: req.params.id,
            name: req.body.name,
            description: req.body.description
        }, function (err, data) {
            if (!err) {
                res.send({ tag: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

module.exports = router;
