const express = require('express');
const router = express.Router();

const feedbackService = require('../services/feedback-service');

router.get('/feedback', function (req, res) {
    if (req.body.companyId) {
        feedbackService.getAll(function (err, data) {
            if (!err) {
                res.send({ feedbacks: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.get('/feedback/:id', function (req, res) {
    if (req.params.id) {
        feedbackService.get(req.params.id, function (err, data) {
            if (!err) {
                res.send({ feedback: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.post('/feedback', function (req, res) {
    if (
        req.body.name &&
        req.body.message &&
        req.body.actions
    ) {
        feedbackService.create({
            name: req.body.name,
            message: req.body.message,
            actions: req.body.actions
        }, function (err, data) {
            if (!err) {
                res.send({ feedback: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.put('/feedback/:id', function (req, res) {
    if (req.params.id && req.body.feedback) {
        feedbackService.create({
            name: req.body.feedback.name,
            text: req.body.feedback.text,
            buttons: []
        }, function (err, data) {
            if (!err) {
                res.send({ group: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.delete('/feedback/:id', function (req, res) {
    if (req.params.id) {
        feedbackService.delete(req.params.id, function (err, data) {
            if (!err) {
                res.send(data);
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

module.exports = router;
