const express = require('express');
const router = express.Router();

const templateService = require('../services/template-service');

router.get('/template', function (req, res) {
    if (req.query.company_id) {
        templateService.getAll(req.query.company_id, function (err, data) {
            if (!err) {
                res.send({ templates: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.get('/template/:id', function (req, res) {
    if (req.params.id && req.params.company_id) {
        templateService.get(req.params.id, function (err, data) {
            if (!err) {
                res.send({ template: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.post('/template', function (req, res) {
    if (
        req.body.company_id &&
        req.body.name &&
        req.body.description &&
        req.body.text
    ) {
        templateService.create({
            company_id: req.body.company_id,
            description: req.body.description,
            name: req.body.name,
            notes: (req.body.notes) ? req.body.notes : "",
            text: req.body.text,
        }, function (err, data) {
            if (!err) {
                res.send({ template: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.delete('/template/:id', function (req, res) {
    if (req.params.id) {
        templateService.delete(req.params.id, function (err, data) {
            if (!err) {
                res.send(data);
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.put('/template/:id', function (req, res) {
    if (req.params.id && req.body.company_id) {
        templateService.update({
            id: req.params.id,
            company_id: req.body.company_id,
            description: req.body.description,
            name: req.body.name,
            notes: (req.body.notes) ? req.body.notes : "",
            text: req.body.text,
        }, function (err, data) {
            if (!err) {
                res.send({ template: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

module.exports = router;
