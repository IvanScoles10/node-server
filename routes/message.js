const _ = require('lodash');
const express = require('express');
const router = express.Router();

const messageService = require('../services/message-service');
let filterBy = {};

router.post('/company/:company_id/message', function (req, res) {
    if (req.params.company_id) {
        _.extend(filterBy, {
            company_id: req.params.company_id,
        });
        if (req.body.filterBy) {
            _.extend(filterBy, req.body.filterBy);
        };

        messageService.getAll(filterBy, function (err, data) {
            if (!err) {
                res.send(data);
            } else {
                res.send(err);
            }
        });
    }
});

router.get('/company/:company_id/message/:id', function (req, res) {
    if (req.params.company_id && req.params.id) {
        messageService.getById(req.params.id, function (err, data) {
            if (!err) {
                res.send(data);
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.post('/message', function (req, res) {
    if (req.body.id) {
        messageService.sendMessage(req.body, function (err, response) {
            if (!err) {
                res.send({ message: response });
            } else {
                res.send({ error: { code: 'error:messages:sendMessage' } });
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.post('/messages/:id/status/:status', function (req, res) {
    if (
        req.params.id && 
        req.params.status
    ) {
        messageService.updateStatusById({
            id: req.params.id,
            status: req.params.status
        }, function (err, data) {
            if (!err) {
                return res.send(data);
            } else {
                return res.send({ error: { code: 'error:messages:updateStatusById' } });
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.post('/message/:id/tags', function (req, res) {
    if (
        req.params.id && 
        req.body.tag
    ) {
        messageService.updateTagById({
            id: req.params.id,
            tag: req.body.tag
        }, function (err, data) {
            if (!err) {
                return res.send(data);
            } else {
                return res.send({ error: { code: 'error:messages:updateTagById' } });
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.post('/message/:id/group', function (req, res) {
    if (
        req.params.id && 
        req.body.group
    ) {
        messageService.updateGroupById({
            id: req.params.id,
            group: req.body.group
        }, function (err, data) {
            if (!err) {
                res.send(data);
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.post('/message/:id/owner', function (req, res) {
    if (
        req.params.id && 
        req.body.owner
    ) {
        messageService.updateOwnerById({
            id: req.params.id,
            owner: req.body.owner
        }, function (err, data) {
            if (!err) {
                res.send(data);
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

module.exports = router;
