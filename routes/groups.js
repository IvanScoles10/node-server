const express = require('express');
const router = express.Router();

const groupsMongo = require('../mongo/groups-mongo');
const groupsService = require('../services/groups-service');

router.get('/groups', function (req, res) {
    groupsService.getAll(function (err, data) {
        if (!err) {
            res.send({ groups: data });
        } else {
            res.send(err);
        }
    });
});

router.get('/groups/:id', function (req, res) {
    if (req.params.id) {
        groupsService.get(req.params.id, function (err, data) {
            if (!err) {
                res.send({ group: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.post('/groups', function (req, res) {
    if (req.body.name) {
        groupsService.create({
            name: req.body.name,
            users: []
        }, function (err, data) {
            if (!err) {
                res.send({ group: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.delete('/groups/:id', function (req, res) {
    if (req.params.id) {
        groupsService.delete(req.params.id, function (err, data) {
            if (!err) {
                res.send(data);
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.put('/groups/:id', function (req, res) {
    if (req.params.id && req.body.user) {
        groupsService.updateUsers({
            id: req.params.id,
            user: req.body.user
        }, function (err, data) {
            if (!err) {
                res.send({ group: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.delete('/groups/:id/:userId', function (req, res) {
    if (req.params.id && req.params.userId) {
        groupsService.deleteUsers({
            id: req.params.id,
            userId: req.params.userId
        }, function (err, data) {
            if (!err) {
                res.send({ group: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

module.exports = router;
