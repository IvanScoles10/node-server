const _ = require('lodash');
const express = require('express');
const router = express.Router();
const messagesService = require('../services/messages-service');

router.get('/analytics', function (req, res) {
    messagesService.getCountsByStatus(function (err, response) {
        if (!err) {

            res.json({
                data: {
                    status: [
                        {
                            name: 'Open',
                            quantity: _.get(_.find(response, { _id : 'open' }), 'count', 0)
                        },
                        {
                            name: 'In Progress',
                            quantity: _.get(_.find(response, { _id : 'in-progress' }), 'count', 0)
                        },
                        {
                            name: 'Closed',
                            quantity: _.get(_.find(response, { _id : 'closed' }), 'count', 0)
                        }
                    ],
                    groups: [
                        {
                            name: 'Marketing',
                            users: 2
                        },
                        {
                            name: 'Operations',
                            users: 1
                        },
                        {
                            name: 'Sales',
                            users: 1
                        }
                    ]
                }
            });
        }
    });
});

module.exports = router;
