const express = require('express');
const router = express.Router();

const auth0Service = require('../services/auth0-service');

router.get('/users', function (req, res) {
    auth0Service.getUsers().then(function (data) {
        res.send(data);
    }).catch(function (err) {
        res.status(200).send({ error: err });
    });
});

router.delete('/users/:id', function (req, res) {
    if (req.params.id) {
        auth0Service.deleteUser(req.params.id).then(function (data) {
            res.status(200).send({ success: true });
        }).catch(function (err) {
            res.status(200).send({ error: err });
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.post('/users', function (req, res) {
    if (
        req.body.email &&
        req.body.password &&
        req.body.username &&
        req.body.user_metadata
    ) {
        auth0Service.createUser(req.body, function (err, data) {
            if (!err) {
                res.send(data);
            } else {
                res.status(200).send({ error: err });
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.get('/users/:id', function(req, res) {
    if (req.params.id) {

    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

module.exports = router;
