const express = require('express');
const router = express.Router();

const categoryService = require('../services/category-service');

router.get('/category', function (req, res) {
    if (req.query.company_id) {
        categoryService.getAll(req.query.company_id, function (err, data) {
            if (!err) {
                res.send({ categories: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.get('/category/:id', function (req, res) {
    if (req.params.id && req.params.company_id) {
        categoryService.get(req.params.id, function (err, data) {
            if (!err) {
                res.send({ category: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.post('/category', function (req, res) {
    if (
        req.body.company_id &&
        req.body.name &&
        req.body.description &&
        req.body.keywords
    ) {
        categoryService.create({
            company_id: req.body.company_id,
            description: req.body.description,
            name: req.body.name,
            notes: (req.body.notes) ? req.body.notes : "",
            users: (req.body.users) ? req.body.users : [],
            keywords: req.body.keywords,
        }, function (err, data) {
            if (!err) {
                res.send({ category: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.delete('/category/:id', function (req, res) {
    if (req.params.id) {
        categoryService.delete(req.params.id, function (err, data) {
            if (!err) {
                res.send(data);
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.put('/category/:id', function (req, res) {
    if (req.params.id && req.body.company_id) {
        categoryService.update({
            id: req.params.id,
            company_id: req.body.company_id,
            description: req.body.description,
            name: req.body.name,
            notes: (req.body.notes) ? req.body.notes : "",
            users: (req.body.users) ? req.body.users : [],
            keywords: req.body.keywords,
        }, function (err, data) {
            if (!err) {
                res.send({ category: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

router.delete('/category/:id/:user_id', function (req, res) {
    if (req.params.id && req.params.user_id) {
        categoryService.deleteUsers({
            id: req.params.id,
            user_id: req.params.user_id
        }, function (err, data) {
            if (!err) {
                res.send({ category: data });
            } else {
                res.send(err);
            }
        });
    } else {
        res.send({ error: { code: 'invalid_parameters' } });
    }
});

module.exports = router;
