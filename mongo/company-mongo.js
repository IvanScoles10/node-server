var instanceDatabase = require('./instance-database').instanceDatabase;
var ObjectID = require('mongodb').ObjectID;
var companyCollection = null;

var companyMongo = {

    getAll: function (company_id, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                companyCollection = db.collection('company');

                companyCollection.find({ 'company_id': company_id }).toArray(function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    create: function (data, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                companyCollection = db.collection('company');

                companyCollection.insert(data, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    delete: function (id, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                companyCollection = db.collection('company');

                companyCollection.remove({ _id:  new ObjectID(id) }, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    update: function (data, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                companyCollection = db.collection('company');

                companyCollection.update(
                    { _id: new ObjectID(data.id) },
                    {
                        $set: {
                            description: data.description,
                            name: data.name,
                            notes: data.notes,
                            keywords: data.keywords,
                        }
                    },
                    function (err, response) {
                        db.close();

                        if (!err) {
                            return cb(null, response);
                        } else {
                            return cb(err);
                        }
                    }
                );
            }
        });
    },

    get: function (id, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                companyCollection = db.collection('company');

                companyCollection.findOne({ _id:  new ObjectID(id) }, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    getByName: function (name, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                companyCollection = db.collection('company');

                companyCollection.findOne({ 'name': name }, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    getByFacebookPageId: function (facebook_page_id, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                companyCollection = db.collection('company');

                companyCollection.findOne({ 'facebook_page_id': facebook_page_id }, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },
};

module.exports = companyMongo;
