var instanceDatabase = require('./instance-database').instanceDatabase;
var ObjectID = require('mongodb').ObjectID;
var feedbackCollection = null;

var feedbackMongo = {

    getAll: function (cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                feedbackCollection = db.collection('feedback');

                feedbackCollection.find().toArray(function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    create: function (data, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                feedbackCollection = db.collection('feedback');

                feedbackCollection.insert(data, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    delete: function (id, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                feedbackCollection = db.collection('feedback');

                feedbackCollection.remove({ _id:  new ObjectID(id) }, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    get: function (id, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                feedbackCollection = db.collection('feedback');

                feedbackCollection.findOne({ _id:  new ObjectID(id) }, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    getByName: function (name, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                feedbackCollection = db.collection('feedback');

                feedbackCollection.findOne({ 'name': name }, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    }
};

module.exports = feedbackMongo;
