const _ = require('lodash');
const mongoose = require("mongoose");
const ObjectID = require("mongodb").ObjectID;

const env = require("../environment/env");
const MessageSchema = require("./message-schemma");

mongoose.connect(env.mongo.DB);

const messageMongo = {

    getAll: function (filterBy, cb) {
        const Message = mongoose.model("Message", MessageSchema, "message");

        Message.find(filterBy).exec(function (err, messages) {
            if (!err) {
                return cb(null, messages);
            } else {
                return cb(err);
            }
        });
    },

    create: function (data, cb) {
        const Message = mongoose.model("Message", MessageSchema, "message");

        Message.create(data, function (err, message) {
            if (!err) {
                return cb(null, message);
            } else {
                return cb(err);
            }
        });
    },

    update: function (data, cb) {
        const Message = mongoose.model("Message", MessageSchema, "message");

        Message.findOneAndUpdate({ _id: new ObjectID(data._id) }, _.pick(data, [
            "messages",
            "read_by_recipient",
            "recipient",
            "sender",
            "status",
        ]), function (err, message) {
            if (!err) {
                return cb(null, message);
            } else {
                return cb(err);
            }
        });
    },

    get: function (id, cb) {
        const Message = mongoose.model("Message", MessageSchema, "message");

        Message.findOne({ _id: new ObjectID(id) }, function (err, message) {
            if (!err) {
                return cb(null, message);
            } else {
                return cb(err);
            }
        });
    },

    getBySenderId: function (senderId, cb) {
        const Message = mongoose.model("Message", MessageSchema, "message");

        Message.findOne({ "sender.id":  senderId }, function (err, message) {
            console.log("entro", message);
            if (!err) {
                return cb(null, message);
            } else {
                return cb(err);
            }
        });
    },

    updateStatusById: function (data, cb) {
        const Message = mongoose.model("Message", MessageSchema, "message");

        Message.findOneAndUpdate({ _id: new ObjectID(data._id) }, {
            $set: {
                status: data.status
            }
        }, function (err, message) {
            if (!err) {
                return cb(null, message);
            } else {
                return cb(err);
            }
        });
    },

    updateTagById: function (data, cb) {
        const Message = mongoose.model("Message", MessageSchema, "message");

        Message.findOneAndUpdate({ _id: new ObjectID(data._id) }, {
            $set: {
                tag: data.tag
            }
        }, function (err, message) {
            if (!err) {
                return cb(null, message);
            } else {
                return cb(err);
            }
        });
    },

    updateGroupById: function (data, cb) {
        const Message = mongoose.model("Message", MessageSchema, "message");

        Message.findOneAndUpdate({ _id: new ObjectID(data._id) }, {
            $set: {
                group: data.group
            }
        }, function (err, message) {
            if (!err) {
                return cb(null, message);
            } else {
                return cb(err);
            }
        });
    },

    updateOwnerById: function (data, cb) {
        const Message = mongoose.model("Message", MessageSchema, "message");

        Message.findOneAndUpdate({ _id: new ObjectID(data._id) }, {
            $set: {
                owner: data.owner
            }
        }, function (err, message) {
            if (!err) {
                return cb(null, message);
            } else {
                return cb(err);
            }
        });
    },

    getCountsByStatus: function (cb) {
        // instanceDatabase(function (err, db) {
        //     if (!err) {
        //         messageCollection = db.collection("message");

        //         messageCollection.aggregate([
        //             {
        //                 "$group": {
        //                     _id: {
        //                         status: "$status"
        //                     },
        //                     count: { "$sum": 1 }
        //                 }},
        //                 {
        //                     "$group": {
        //                         _id: "$_id.status",
        //                         count: { "$sum": "$count" }
        //                     }
        //                 }

        //         ], function (err, response) {
        //             db.close();

        //             if (!err) {
        //                 return cb(null, response);
        //             } else {
        //                 return cb(err);
        //             }
        //         });
        //     }
        // });
    }
};

module.exports = messageMongo;
