var instanceDatabase = require('./instance-database').instanceDatabase;
var ObjectID = require('mongodb').ObjectID;
var groupsCollection = null;

var groupsMongo = {

    getAll: function (cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                groupsCollection = db.collection('groups');

                groupsCollection.find().toArray(function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    create: function (data, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                groupsCollection = db.collection('groups');

                groupsCollection.insert(data, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    delete: function (id, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                groupsCollection = db.collection('groups');

                groupsCollection.remove({ _id:  new ObjectID(id) }, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    get: function (id, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                groupsCollection = db.collection('groups');

                groupsCollection.findOne({ _id:  new ObjectID(id) }, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    getByName: function (name, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                groupsCollection = db.collection('groups');

                groupsCollection.findOne({ 'name': name }, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    updateUsers: function (data, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                groupsCollection = db.collection('groups');

                groupsCollection.update(
                    { _id: new ObjectID(data.id) },
                    {
                        $push: {
                            users: data.user
                        }
                    }, function (err, response) {
                        db.close();

                        if (!err) {
                            return cb(null, response);
                        } else {
                            return cb(err);
                        }
                    }
                );
            }
        });
    },

    deleteUsers: function (data, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                groupsCollection = db.collection('groups');

                groupsCollection.update(
                    { _id: new ObjectID(data.id) },
                    {
                        $pop: {
                            users: {
                                user_id: data.userId
                            }
                        }
                    }, function (err, response) {
                        db.close();

                        if (!err) {
                            return cb(null, response);
                        } else {
                            return cb(err);
                        }
                    }
                );
            }
        });
    }
};

module.exports = groupsMongo;
