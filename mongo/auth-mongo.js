var instanceDatabase = require('./instance-database').instanceDatabase;
var auth0Collection = null;

var authMongo = {

    getAll: function (cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                auth0Collection = db.collection('auth0');

                auth0Collection.findOne(
                    { token_type:  'Bearer' },
                    function (err, response) {
                        db.close();

                        if (!err) {
                            return cb(null, response);
                        } else {
                            return cb(err);
                        }
                    }
                );
            }
        });
    },

    create: function (data, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                auth0Collection = db.collection('auth0');

                auth0Collection.insert(data, function(err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    update: function (data, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                auth0Collection = db.collection('auth0');

                auth0Collection.findOneAndUpdate(
                    { token_type: 'Bearer' },
                    {
                        $set: {
                            access_token: data.access_token,
                            expires_in: data.expires_in,
                            scope: data.scope
                        }
                    },
                    function (err, response) {
                        db.close();

                        if (!err) {
                            return cb(null, response);
                        } else {
                            return cb(err);
                        }
                    }
                );
            }
        });
    }
};

module.exports = authMongo;
