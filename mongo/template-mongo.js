const mongoose = require("mongoose");
const ObjectID = require("mongodb").ObjectID;

const env = require("../environment/env");
const TemplateSchema = require("./template-schemma");

mongoose.connect(env.mongo.DB);

var templateMongo = {

    getAll: function (company_id, cb) {
        const Template = mongoose.model("template", TemplateSchema, "template");

        Template.find({ 'company_id': company_id }).exec(function (err, templates) {
            if (!err) {
                return cb(null, templates);
            } else {
                return cb(err);
            }
        });
    },

    create: function (data, cb) {
        const Template = mongoose.model("template", TemplateSchema, "template");

        Template.create(data, function (err, template) {
            if (!err) {
                return cb(null, template);
            } else {
                return cb(err);
            }
        });
    },

    delete: function (id, cb) {
        const Template = mongoose.model("template", TemplateSchema, "template");

        Template.remove({ _id: new ObjectID(id) }).exec(function (err, template) {
            if (!err) {
                return cb(null, template);
            } else {
                return cb(err);
            }
        });
    },

    update: function (data, cb) {
        const Template = mongoose.model("template", TemplateSchema, "template");
        
        Template.findOneAndUpdate({ _id: new ObjectID(data.id) }, {
            $set: {
                description: data.description,
                name: data.name,
                notes: data.notes,
                text: data.text,
            }
        }).exec(function (err, template) {
            if (!err) {
                return cb(null, template);
            } else {
                return cb(err);
            }
        });
    },

    get: function (id, cb) {
        const Template = mongoose.model("template", TemplateSchema, "template");

        Template.findOne({ _id: new ObjectID(id) }).exec(function (err, template) {
            if (!err) {
                return cb(null, template);
            } else {
                return cb(err);
            }
        });
    },

    getByName: function (name, cb) {
        const Template = mongoose.model("template", TemplateSchema, "template");

        Template.findOne({ 'name': name }).exec(function (err, template) {
            if (!err) {
                return cb(null, template);
            } else {
                return cb(err);
            }
        });
    },

};

module.exports = templateMongo;
