const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = new Schema({
    company_id:  String,
    messages: Array,
    sender: {
        id: Number,
        first_name: String,
        last_name: String,
        profile_pic: String,
        locale: String,
        timezone: Number,
        gender: String,
    },
    status: String,
    read_by_recipient: Boolean,
});
