const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = new Schema({
    company_id: String,
    date_created: Number,
    description: String,
    name: String,
    notes: String,
    users: Array,
    keywords: Array
});
