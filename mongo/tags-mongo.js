var instanceDatabase = require('./instance-database').instanceDatabase;
var ObjectID = require('mongodb').ObjectID;
var TagsCollection = null;

var TagsMongo = {

    getAll: function (cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                TagsCollection = db.collection('tags');

                TagsCollection.find().toArray(function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    create: function (data, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                TagsCollection = db.collection('tags');

                TagsCollection.insert(data, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    delete: function (id, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                TagsCollection = db.collection('tags');

                TagsCollection.remove({ _id: new ObjectID(id) }, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    get: function (id, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                TagsCollection = db.collection('tags');

                TagsCollection.findOne({ _id: new ObjectID(id) }, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    },

    getByName: function (name, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                TagsCollection = db.collection('tags');

                TagsCollection.findOne({ 'name': name }, function (err, response) {
                    db.close();

                    if (!err) {
                        return cb(null, response);
                    } else {
                        return cb(err);
                    }
                });
            }
        });
    }
};

module.exports = TagsMongo;
