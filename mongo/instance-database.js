var env = require('../environment/env');
var MongoClient = require('mongodb').MongoClient;

var instanceDatabase = function (cb) {
    MongoClient.connect(env.mongo.DB, function(err, db) {
        if (!err) {
            console.log('Connected correctly to mongodb server.');
            return cb(null, db);
        } else {
            console.log('Error connecting to mongodb server', err);
            return cb(err);
        }
    });
};

module.exports = {
    instanceDatabase: instanceDatabase
};
