const _ = require('lodash');
const mongoose = require("mongoose");
const ObjectID = require("mongodb").ObjectID;

const env = require("../environment/env");
const CategorySchema = require("./category-schemma");

mongoose.connect(env.mongo.DB);

const categoryMongo = {

    getAll: function (company_id, cb) {
        const Category = mongoose.model("category", CategorySchema, "category");

        Category.find({ 'company_id': company_id }).exec(function (err, categories) {
            if (!err) {
                return cb(null, categories);
            } else {
                return cb(err);
            }
        });
    },

    create: function (data, cb) {
        const Category = mongoose.model("category", CategorySchema, "category");

        Category.create(data, function (err, category) {
            if (!err) {
                return cb(null, category);
            } else {
                return cb(err);
            }
        });
    },

    delete: function (id, cb) {
        const Category = mongoose.model("category", CategorySchema, "category");

        Category.remove({ _id: new ObjectID(id) }).exec(function (err, category) {
            if (!err) {
                return cb(null, category);
            } else {
                return cb(err);
            }
        });
    },

    update: function (data, cb) {
        const Category = mongoose.model("category", CategorySchema, "category");

        Category.findOneAndUpdate({ _id: new ObjectID(data.id) }, {
            $set: {
                description: data.description,
                name: data.name,
                notes: data.notes,
                keywords: data.keywords,
            }
        }).exec(function (err, category) {
            if (!err) {
                return cb(null, category);
            } else {
                return cb(err);
            }
        });
    },

    get: function (id, cb) {
        const Category = mongoose.model("category", CategorySchema, "category");

        Category.findOne({ _id: new ObjectID(id) }).exec(function (err, category) {
            if (!err) {
                return cb(null, category);
            } else {
                return cb(err);
            }
        });
    },

    getByName: function (name, cb) {
        const Category = mongoose.model("category", CategorySchema, "category");

        Category.findOne({ 'name': name }).exec(function (err, category) {
            if (!err) {
                return cb(null, category);
            } else {
                return cb(err);
            }
        });
    },
};

module.exports = categoryMongo;
