const _ = require('lodash');
var instanceDatabase = require('./instance-database').instanceDatabase;
var analyticsCollection = null;
var ObjectID = require('mongodb').ObjectID;

var analyticsMongo = {

    updateFeedback: function (data, cb) {
        instanceDatabase(function (err, db) {
            if (!err) {
                analyticsCollection = db.collection('analytics');

                analyticsCollection.findAndModify(
                    { 'analytics.messages.type': 'messages' },
                    [[ '_id', 'asc' ]],
                    {
                        $set: {
                            analytics: {
                                messages: {
                                    type: 'messages',
                                    open: data.open,
                                    'in-progress': data.inProgress,
                                    closed: data.closed
                                }
                            }
                        }
                    },
                    {
                        new: false,
                        upsert: false
                    },
                    function (err, response) {
                        db.close();

                        if (!err) {
                            return cb(null, response);
                        } else {
                            return cb(err);
                        }
                    }
                );
            }
        });
    }
};

module.exports = analyticsMongo;
